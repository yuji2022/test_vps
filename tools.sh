#缝合怪在此
RED="\e[1;31m"
GREEN="\e[1;32m"
OTHER="\e[1;$[RANDOM%7+31]m"
END="\e[0m"
#检查是否是root用户运行
[[ "`id -u`" != "0" ]] && echo -e "${RED}这边建议以root用户运行脚本${END}" && exit 1
while :;do
echo -e "${OTHER}
菜单：
  ${RED}q  ---  退出脚本${END}
--------------------------------
--------------------------------
  ${OTHER}1  ---  bbr脚本
  2  ---  使用ACME申请证书
--------------------------------
  3  ---  流媒体检测
  4  ---  warp-go
  5  ---  查看ipv4/ipv6优先
--------------------------------
  6  ---  hysteria
  7  ---  安装原版x-ui
  8  ---  安装魔改版x-ui
--------------------------------
  9  ---  测试三网回程路由
  10 ---  测试小鸡性能（磁盘和网络）
${END}
"
read -e -p "请输入：" INPUT
  case $INPUT in
    q|Q)
      break;;
    1)
      bash <(curl -fsSL https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh);;
    2)
      bash <(curl https://raw.githubusercontent.com/yuji01/vps/main/acme_yuji01.sh);;
    3)
      bash <(curl -L -s check.unlock.media);;
    4)
      bash <(curl -fsSL https://raw.githubusercontent.com/fscarmen/warp/main/warp-go.sh);;
    5)
      echo -e "你访问网站的默认ip地址为：${OTHER}`curl ip.sb`${END} 请自行判断其属于 ipv4/ipv6";;
    6)
      bash <(curl -fsSL https://git.io/hysteria.sh);;
    7)
      bash <(curl -Ls https://raw.githubusercontent.com/vaxilu/x-ui/master/install.sh);;
    8)
      bash <(curl -Ls https://raw.githubusercontent.com/FranzKafkaYu/x-ui/master/install.sh);;
    9)
      bash <(curl https://raw.githubusercontent.com/yuji01/vps/main/backtrace.sh);;
    10)
      curl -sL yabs.sh | bash;;
    *)
      echo -e "${RED}请重新输入${END}"
  esac
done
