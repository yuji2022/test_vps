#!/bin/bash
#感谢https://github.com/zhanghanyun/backtrace提供的项目，我只是加了一个判断
if [[ ! -f /usr/bin/backtrace ]];then
  arch=$(uname -m)
  if [ "$arch" = "x86_64" ]; then
    wget -q -O backtrace.tar.gz  https://github.com/zhanghanyun/backtrace/releases/latest/download/backtrace-linux-amd64.tar.gz
  else
    wget -q -O backtrace.tar.gz  https://github.com/zhanghanyun/backtrace/releases/latest/download/backtrace-linux-arm64.tar.gz
  fi
  tar -xf backtrace.tar.gz
  rm backtrace.tar.gz
  mv backtrace /usr/bin/
fi
backtrace
