#!/bin/bash
RED="\e[1;31m"
GREEN="\e[1;32m"
OTHER="\e[1;$[RANDOM%7+31]m"
END="\e[0m"
#判断ipv4/ipv6优先
result=`curl ip.sb`
if [[ $result =~ "\." ]];then
  for i in {1..3};do
    echo -e "${RED}网络ipv4优先${END}"
  done
else
  for i in {1..3};do
    echo -e "${GREEN}网络ipv6优先${END}"
  done
fi
